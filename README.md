# linux



## Comandos Linux

ls -a = lista os arquivos ocultos.

`ls path/*.extensao` = lista todos os arquivos com aquela extensão, ls/doc/*.conf.


ls path/* X * = lista todos os aqruivos que tenha o texto especificado.

cd ~ = voltara para a pasta raiz do usuario.

cd / = volta para a para raiz do sistema.

pwd = mostra o caminho absoluto da pasta.

mkdir = cria a pasta.

rmdir = deletar pasta.

rm -rf nomeDaPasta = deleta a pasta com todos os arquivos dentro.

touch = cria um arquivo em branco, touch nomeDoArquivo.

cat nomeDoArquivo =  exibe o conteudo do arquivo.

tac nomeDoArquivo =  exibe o conteudo do arquivo mas fo fim para o começo.

vim nomeDoArquivo = abre o arquivo. I para poder fazer insert, ESC para ir para poder digitar comandos, wq para salvar e sair.

rm nomeDoArquivo = deleta arquivo.

cp nomeDoArquivo pastaDestino = copia o arquivo para a pasta destino, -u copia apenas os arquivos não existente ou mais novos para a pasta destino.

cp dir1/* dir2 = Using a wildcard, copy all the files in dir1 into dir2. The directory dir2 must already exist.

mv nomeDoArquivo pastaDestino = move o arquivo para a pasta destino.

less nomeDoArquivo= exibi o arquivo em paginação. o barra(/) entra no modo de busca no arquivo. N pula para as próximas referencia do find.

tail nomeDoArquivo = exibe a parte final do arquivo.

find . -name nomeDoArquivo.txt = busca a partir do diretorio atual o arquivo informado.

`find . -name *txt*` = busca todos os aqruivos que tem txt no nome.

`>>` = pega a saida de um comando para inserir dentro de um arquivo, ex: whoami >> meuArquivo.txt

grep 'o que vc quer filtrar' nomeDoArquivo = filtrar coisas em arquivos.

diff nomeDoArquivo1 nomeDoArquivo2 = mostrra a diferença entre os arquivos.
